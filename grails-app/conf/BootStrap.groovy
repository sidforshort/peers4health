import org.jnanasanjeevini.*
import grails.converters.JSON

class BootStrap {

    def init = { servletContext ->

    	// Create the roles if they don't exist
        ['ROLE_CHW','ROLE_DATA','ROLE_USER','ROLE_ADMIN','ROLE_ANONYMOUS'].each { roleStr ->
            if (!Role.findByAuthority(roleStr)) {
                new Role(authority: roleStr).save(flush: true)
            }
        }
    }
    
    def destroy = {
    }
}
