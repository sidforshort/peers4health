modules = {
    application {
        resource url:'js/application.js'
    }

    jQuery {
        resource url: "js/jquery-1.10.2.min.js"
    }

    bootstrap {
        resource url:'css/bootstrap.min.css',attrs:[rel: "stylesheet/less", type:'css']
        dependsOn 'jquery'
        dependsOn 'bootstrapJs'
    }
 
    bootstrapJs {
        resource url:'js/affix.js'
        resource url:'js/alert.js'
        resource url:'js/button.js'
        resource url:'js/carousel.js'
        resource url:'js/collapse.js'
        resource url:'js/dropdown.js'
        resource url:'js/modal.js'
        resource url:'js/popover.js'
        resource url:'js/scrollspy.js'
        resource url:'js/tab.js'
        resource url:'js/tooltip.js'
        resource url:'js/transition.js'
        //resource url:'js/typeahead.js'
    }
}