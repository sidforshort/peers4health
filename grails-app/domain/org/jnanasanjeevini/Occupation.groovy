package org.jnanasanjeevini

class Occupation {

	String occupationName

    static constraints = {
    	occupationName nullable:false
    }
}
