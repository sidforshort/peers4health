package org.jnanasanjeevini

class FamilyHead extends Person {

	int numberOfChildren
	int numberOfGrandChildren
	Date dateEnrolled
    String spouseAlive

    static constraints = {
    	numberOfChildren nullable:true, min:0, max:100
    	numberOfGrandChildren nullable:true, min:0, max:100
    	dateEnrolled blank:false
		spouseAlive nullable:true, inList:["Y","N"]
    }
}
