package org.jnanasanjeevini

class Family {

	String assignedId
	String address
	String locationType
	String landlineNumber
	String annualFamilyIncome
	String consentForScreening

    static constraints = {
    	assignedId nullable:true, unique:true
    	landlineNumber nullable:true, unique:true
		address nullable:true
		annualFamilyIncome nullable:true, inList:["Less than ₹ 50,000", "₹ 50,001 - ₹ 1,00,000", "₹ 1,00,001 - ₹ 5,00,000", "More than ₹ 5,00,000"]
		locationType nullable:true, inList:["Rural", "Semi Urban", "Urban"]
		consentForScreening nullable:false, inList:["Y", "N"]
    }

    static hasMany = [persons: Person]
    static belongsTo = [user: User]

    def afterInsert() {
		createId()
	}

	def afterUpdate() {
		if (isDirty('assignedId')) {
			createId()
		}
	}

	protected void createId() {
		assignedId = "${user.profile.assignedId}/FAM-${this.id}"
	}
}
