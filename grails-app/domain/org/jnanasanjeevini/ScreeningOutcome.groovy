package org.jnanasanjeevini

class ScreeningOutcome {

	Encounter encounter
	String dmTreatment
    String htTreatment
	String screeningAction
	String nextAppointment
	String comments
    String diabetesPrognosis
    String hypertensionPrognosis

    static constraints = {
    	dmTreatment nullable:false, inList:["None", "Tablets", "Insulin"]
        htTreatment nullable:false, inList:["None", "Tablets"]
    	screeningAction nullable:false, inList:["Normal: health counselling", "Borderline Diabetes: health counselling", "Borderline Hypertension: health counselling", "Diabetes: refer medical doctor", "Hypertension: refer medical doctor", "Urgent medical care", "Emergency hospitalization", "Follow-up with private doctor", "Follow-up with Govt. hospital", "Follow-up with JS", "Other"]
    	nextAppointment nullable:false, inList:["Confirmation", "Monthly Medical Review", "Quarterly Medical Review", "Annual Medical Review", "Hospital Admission", "Annual Rescreening", "Other"]
    	comments nullable:true, size:0..3000
        diabetesPrognosis nullable:true, inList:["Normal", "Diabetic", "Borederline Diabetic", "Unsure"]
        hypertensionPrognosis nullable:true, inList:["Dangerously Low", "Low", "Low Normal", "Normal", "Prehypertension", "High - Stage 1", "High - Stage 2", "High - Stage 3", "High - Stage 4", "Unsure"]
    }

    def beforeInsert() {
        prognosticate()
    }

    def beforeUpdate() {
        /*if (isDirty('dmTreatment')) {
            prognosticate()
        }*/
    }

    protected void prognosticate() {

        def hypertensionExam = encounter.hypertensionExam
        def systolicReading = hypertensionExam.getSystolicReading()
        def diastolicReading = hypertensionExam.getDiastolicReading()

        if (systolicReading < 50 && diastolicReading < 33) {
            hypertensionPrognosis = "Dangerously Low"

        } else if ((systolicReading > 50 && systolicReading < 90) && (diastolicReading > 33 && diastolicReading < 60)) {
            hypertensionPrognosis = "Low"

        } else if ((systolicReading > 90 && systolicReading < 100) && (diastolicReading > 60 && diastolicReading < 65)) {
            hypertensionPrognosis = "Low Normal"

        } else if ((systolicReading > 100 && systolicReading < 130) && (diastolicReading > 65 && diastolicReading < 85)) {
            hypertensionPrognosis = "Normal"
        
        } else if ((systolicReading > 130 && systolicReading < 140) && (diastolicReading > 85 && diastolicReading < 90)) {
            hypertensionPrognosis = "Prehypertension"
        
        } else if ((systolicReading > 140 && systolicReading < 160) && (diastolicReading > 90 && diastolicReading < 100)) {
            hypertensionPrognosis = "High - Stage 1"
        
        } else if ((systolicReading > 160 && systolicReading < 180) && (diastolicReading > 100 && diastolicReading < 110)) {
            hypertensionPrognosis = "High - Stage 2"
        
        } else if ((systolicReading > 180 && systolicReading < 210) && (diastolicReading > 110 && diastolicReading < 120)) {
            hypertensionPrognosis = "High - Stage 3"
        
        } else if (systolicReading > 210 && diastolicReading > 120) {
            hypertensionPrognosis = "High - Stage 4"
        
        } else {
            hypertensionPrognosis = "Unsure"
        }


        def diabetesExam = encounter.diabetesExam

        if (diabetesExam.fastingReading < 100 && diabetesExam.pm2hReading < 140) {
            diabetesPrognosis = "Normal"

        } else if ((diabetesExam.fastingReading >= 126 && diabetesExam.pm2hReading >= 200) || (diabetesExam.randomReading >= 200)) {
            diabetesPrognosis = "Diabetic"

        } else if ((diabetesExam.iftReading >= 101 && diabetesExam.iftReading <= 125) || (diabetesExam.igtReading >= 141 && diabetesExam.igtReading <= 199)) {
            diabetesPrognosis = "Borederline Diabetic"
        
        } else {
            diabetesPrognosis = "Unsure"
        }

    }
}
