package org.jnanasanjeevini

class User {

	transient springSecurityService

	String email
	String password

	boolean enabled = false
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	Date dateCreated
	Date lastUpdated

	static transients = ['springSecurityService']

	static constraints = {
		email blank: false, unique: true, email: true
		password (blank: false, validator: { passwd, usr ->
            return passwd != usr.email
        })
        profile nullable:true
	}

	static mapping = {
		password column: '`password`'
	}

	static hasOne = [profile: Profile]

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}
