package org.jnanasanjeevini

class Encounter {

	String encounterType

    static constraints = {
    	encounterType nullable:false, inList:["Initial Screening", "Confirmation", "Annual Rescreening", "Review"]
        physicalExam nullable:true
    	diabetesExam nullable:true
    	hypertensionExam nullable:true
    	screeningOutcome nullable:true
    }

    static belongsTo = [person: Person]
    static hasOne = [physicalExam: PhysicalExamination, diabetesExam: DiabetesExamination, hypertensionExam: HypertensionExamination, screeningOutcome: ScreeningOutcome]
}
