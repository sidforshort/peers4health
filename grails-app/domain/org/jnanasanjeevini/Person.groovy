package org.jnanasanjeevini

class Person {

	String assignedId
	String jsmcId
	String uhId
	String firstName
	String lastName
	Date dateEnrolled
	Date yearOfBirth
	String gender
	String mobileNumber
	String aadharID
	String educationLevel
	Occupation occupation

    static constraints = {
		assignedId nullable:true, unique:true
		jsmcId nullable:true, unique:true
		uhId nullable:true, unique:true
		dateEnrolled blank:false
		firstName blank:false
		lastName blank:false
		yearOfBirth nullable:true
		gender nullable:true, inList:["M","F"]
		mobileNumber nullable:true, unique:true, matches:"^[7-9][0-9]{9}\$"
		aadharID nullable:true
		educationLevel nullable:true, inList:['None', 'Primary Education', 'High School', 'Graduate', 'Post Graduate', 'Vocational Training']
		occupation nullable:true
		medicalHistory nullable:true
    }

    static belongsTo = [family: Family]
    static hasOne = [medicalHistory: MedicalHistory]
    static hasMany = [encounters: Encounter]

    static mapping = {
        tablePerHierarchy false
    }

    def afterInsert() {
		createId()
	}

	def afterUpdate() {
		if (isDirty('assignedId')) {
			createId()
		}
	}

	protected void createId() {
		assignedId = "${family.assignedId}/PER-${this.id}"
	}
}
