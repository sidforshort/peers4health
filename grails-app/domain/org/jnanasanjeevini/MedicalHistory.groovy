package org.jnanasanjeevini

class MedicalHistory {

	String knownDiabetes
	String knownHypertension
	String knownHeartDisease
	String knownStroke
	Person person

    static constraints = {
    	knownDiabetes nullable:false, inList:["Y","N"]
		knownHypertension nullable:false, inList:["Y","N"]
		knownHeartDisease nullable:false, inList:["Y","N"]
		knownStroke nullable:false, inList:["Y","N"]
    }
}
