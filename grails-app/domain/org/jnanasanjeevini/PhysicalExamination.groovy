package org.jnanasanjeevini

class PhysicalExamination {

	Encounter encounter
	int heightInCms
	int waistInCms
	int hipInCms
	float weightInKgs

    static constraints = {
    	heightInCms min:0
    	waistInCms min:0
    	hipInCms min:0
    	weightInKgs min:0F
    }
}
