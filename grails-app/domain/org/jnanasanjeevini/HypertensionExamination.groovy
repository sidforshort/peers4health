package org.jnanasanjeevini

class HypertensionExamination {

	Encounter encounter
	float systolic1A
	float diastolic1A
	float pulseRate1A
	float systolic1B
	float diastolic1B
	float pulseRate1B
	float normalReading
	float preHypertension
	float hypertension

    static constraints = {
    	systolic1A min:0F
    	diastolic1A min:0F
    	pulseRate1A min:0F
    	systolic1B min:0F
    	diastolic1B min:0F
    	pulseRate1B min:0F
    	normalReading min:0F
    	preHypertension min:0F
    	hypertension min:0F
    }

    public float getSystolicReading() {
        
        if (systolic1A >= systolic1B) {
            return systolic1A
        } else {
            return systolic1B
        }
    }

    public float getDiastolicReading() {

        if (diastolic1A >= diastolic1B) {
            return diastolic1A
        } else {
            return diastolic1B
        }
    }
}
