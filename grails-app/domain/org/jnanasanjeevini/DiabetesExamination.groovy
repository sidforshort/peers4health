package org.jnanasanjeevini

class DiabetesExamination {

	Encounter encounter
	float fastingReading
	float pm2hReading
	float randomReading
	float normalReading
	float iftReading
	float igtReading
	float dmReading

    static constraints = {
    	fastingReading min:0F
    	pm2hReading min:0F
    	randomReading min:0F
    	normalReading min:0F
    	iftReading min:0F
    	igtReading min:0F
    	dmReading min:0F
    }
}
