package org.jnanasanjeevini

class Profile {

	String assignedId
	String firstName
	String lastName
	String gender
	Date yearOfBirth
	String category
	String address
	String mobileNumber
	String landlineNumber
	String confirmationToken
	String aadharID
	String educationLevel
	User user
	byte[] photo

	boolean emailConfirmed = false
	boolean mobileConfirmed = false

    static constraints = {

		assignedId nullable:true
		firstName blank:false
		lastName blank:false
		mobileNumber unique:true, matches:"^[7-9][0-9]{9}\$"
		landlineNumber nullable:true
		aadharID nullable:true
		yearOfBirth blank:false
		gender blank:false, inList:["M","F"]
		address blank:false
		category nullable:true, inList:['Type 1 Diabetes - Self', 'Type 1 Diabetes - Mother', 'Type 1 Diabetes - Father', 'Type 1 Diabetes - Relative', 'Type 1 Diabetes - Friend', 'Type 2 Diabetes - Self', 'Type 2 Diabetes - Mother', 'Type 2 Diabetes - Father', 'Type 2 Diabetes - Relative', 'Type 2 Diabetes - Friend', 'Unrelated']
		educationLevel nullable:true, inList:['None', 'Primary Education', 'High School', 'Graduate', 'Post Graduate', 'Vocational Training']
		photo nullable:true
		confirmationToken nullable:true
    }
}
