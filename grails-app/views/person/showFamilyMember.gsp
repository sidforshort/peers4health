<%@ page import="org.jnanasanjeevini.*" %>
<%@ page import="org.joda.time.Period" %>
<%@ page import="org.joda.time.DateTime" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Show Family Member Details | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <g:hasErrors bean="${person}">
                        <g:eachError>
                            <p>
                                <g:message error="${it}"/>
                            </p>
                        </g:eachError>
                    </g:hasErrors>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Person: ${person?.firstName} <span class="label label-default pull-right">${person?.assignedId}</span></h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 hidden-xs hidden-sm" align="center">
                                    <g:img dir="images" file="person.png" alt="Person Pic" width="128" height="128" class="img-circle"/>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="table-responsive">
                                        <table class="table table-user-information">
                                            <tbody>
                                                <tr>
                                                    <td>Person ID:</td>
                                                    <td>${person?.assignedId}</td>
                                                </tr>
                                                <tr>
                                                    <td>Full Name:</td>
                                                    <td>
                                                        ${person?.firstName} ${person?.lastName }
                                                        <g:if test="${person.class.name == 'org.jnanasanjeevini.FamilyHead'}">
                                                            <span class="label label-default">Head of Family</span>
                                                        </g:if>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Education:</td>
                                                    <td>${person?.educationLevel}</td>
                                                </tr>
                                                <tr>
                                                    <td>Occupation:</td>
                                                    <td>${person?.occupation}</td>
                                                </tr>
                                                <tr>
                                                    <td>Gender:</td>
                                                    <td>${person?.gender}</td>
                                                </tr>
                                                <%
                                                    def now = new DateTime()
                                                    def yob = new DateTime(person?.yearOfBirth)
                                                    def period = new Period(yob, now)
                                                %>
                                                <tr>
                                                    <td>Age:</td>
                                                    <td>${period?.years}</td>
                                                </tr>
                                                <tr>
                                                    <td>Aadhar ID:</td>
                                                    <td>${person?.aadharID}</td>
                                                </tr>
                                                <tr>
                                                    <td>JSMC ID:</td>
                                                    <td>${(person?.jsmcId)?:"Not Assigned"}</td>
                                                </tr>
                                                <tr>
                                                    <td>Universal Health ID:</td>
                                                    <td>${(person?.uhId)?:"Not Assigned"}</td>
                                                </tr>
                                                <tr>
                                                    <td>Enrolment Date:</td>
                                                    <td>
                                                        <g:formatDate date="${person?.dateEnrolled}" format="d MMM, yyyy"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enrolled By:</td>
                                                    <td>${person?.family?.user?.profile?.firstName}</td>
                                                </tr>
                                                <tr>
                                                    <td>Mobile Number:</td>
                                                    <td>${person?.mobileNumber}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /panel-body -->
                        <div class="panel-footer">
                            <g:link controller="encounter" action="addEncounter" params="[personId:"${person?.id}"]" class="btn btn-xs btn-block btn-primary">
                                <span class="glyphicon glyphicon-plus-sign"></span> Add Encounter
                            </g:link>
                        </div>
                        <!-- /panel-footer -->
                    </div>
                </div>
                <!-- /panel-container div -->
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 hidden-xs hidden-sm">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Family Details
                            </h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 hidden-xs hidden-sm" align="center">
                                    <g:img dir="images" file="family.png" alt="family" width="96" height="96" class="img-circle"/>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="table-responsive">
                                        <table class="table table-user-information">
                                            <tbody>
                                                <tr>
                                                    <td>Family ID:</td>
                                                    <td>${person?.family?.assignedId}</td>
                                                </tr>
                                                <tr>
                                                    <td>Annual Family Income:</td>
                                                    <td>${person?.family?.annualFamilyIncome}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address:</td>
                                                    <td>${person?.family?.address}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location Type:</td>
                                                    <td>${person?.family?.locationType}</td>
                                                </tr>
                                                <tr>
                                                    <td>Landline Number:</td>
                                                    <td>${person?.family?.landlineNumber}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /panel-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Members of this Family <span class="badge badge-default pull-right">${person?.family?.persons?.size()}</span></h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <g:each in="${person?.family?.persons?.sort()}" var="member">
                                        <g:link action="showFamilyMember" id="${member.id}" class="btn btn-xs">
                                            ${member?.firstName} ${member?.lastName }
                                        </g:link>
                                    </g:each>
                                </div>
                            </div>
                            <!-- /panel-body -->
                        </div>
                        <!-- /panel-container div -->
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>