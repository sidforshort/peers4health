<%@ page import="org.jnanasanjeevini.*" %>
<%@ page import="org.joda.time.Period" %>
<%@ page import="org.joda.time.DateTime" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Add A New Family Member | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <g:hasErrors bean="${person}">
                        <g:eachError>
                            <p>
                                <g:message error="${it}"/>
                            </p>
                        </g:eachError>
                    </g:hasErrors>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">New Member Details <span class="label label-default pull-right">${family?.assignedId}</span></h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 hidden-xs hidden-sm" align="center">
                                    <g:img dir="images" file="family.png" alt="Person Pic" width="96" height="96" class="img-circle"/>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <g:form action="addFamilyMember" class="form-horizontal" role="form">
                                        <g:hiddenField name="familyId" value="${family?.id}" />
                                        <div class="form-group">
                                            <label for="firstName" class="col-sm-3 control-label">Name</label>
                                            <div class="col-sm-5">
                                                <g:textField name="firstName" value="${person?.firstName}" class="form-control" placeholder="First name" required="true" />
                                            </div>
                                            <div class="col-sm-4">
                                                <g:textField name="lastName" value="${person?.lastName}" class="form-control" placeholder="Last name" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="dateEnrolled" class="col-sm-3 control-label">Date Enrolled</label>
                                            <div class="col-sm-9">
                                                <g:datePicker name="dateEnrolled" class="form-control" value="${person?.dateEnrolled}" precision="day" placeholder="Date Enrolled" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="yearOfBirth" class="col-sm-3 control-label">Year of birth</label>
                                            <div class="col-sm-9">
                                                <g:datePicker name="yearOfBirth" class="form-control" value="${person?.yearOfBirth}" precision="year" placeholder="Year of Birth" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="aadharID" class="col-sm-3 control-label">Aadhar ID</label>
                                            <div class="col-sm-9">
                                                <g:textField name="aadharID" value="${person?.aadharID}" class="form-control" placeholder="Aadhar ID" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="educationLevel" class="col-sm-3 control-label">Education</label>
                                            <div class="col-sm-9">
                                                <g:select name="educationLevel" from="['None', 'Primary Education', 'High School', 'Graduate', 'Post Graduate', 'Vocational Training']" noSelection="['':'- Choose Education -']" value="${person?.educationLevel}" class="form-control" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="mobileNumber" class="col-sm-3 control-label">Mobile Phone</label>
                                            <div class="col-sm-9">
                                                <g:textField name="mobileNumber" value="${person?.mobileNumber}" class="form-control" placeholder="Mobile number" type="tel" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender" class="col-sm-3 control-label">Gender</label>
                                            <div class="col-sm-9">
                                                <g:radioGroup name="gender" labels="['Male','Female']" values="['M','F']" required="true">
                                                    <div class="radio col-sm-3">
                                                        <p>${it.label} ${it.radio}</p>
                                                    </div>
                                                </g:radioGroup>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="knownDiabetes" class="col-sm-3 control-label">Known Diabetes?</label>
                                            <div class="col-sm-9">
                                                <g:radioGroup name="knownDiabetes" labels="['Yes','No']" values="['Y','N']" required="true">
                                                    <div class="radio col-sm-3">
                                                        <p>${it.label} ${it.radio}</p>
                                                    </div>
                                                </g:radioGroup>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="knownHypertension" class="col-sm-3 control-label">Hypertension?</label>
                                            <div class="col-sm-9">
                                                <g:radioGroup name="knownHypertension" labels="['Yes','No']" values="['Y','N']" required="true">
                                                    <div class="radio col-sm-3">
                                                        <p>${it.label} ${it.radio}</p>
                                                    </div>
                                                </g:radioGroup>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="knownHeartDisease" class="col-sm-3 control-label">Heart Disease?</label>
                                            <div class="col-sm-9">
                                                <g:radioGroup name="knownHeartDisease" labels="['Yes','No']" values="['Y','N']" required="true">
                                                    <div class="radio col-sm-3">
                                                        <p>${it.label} ${it.radio}</p>
                                                    </div>
                                                </g:radioGroup>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="knownStroke" class="col-sm-3 control-label">Known Stroke?</label>
                                            <div class="col-sm-9">
                                                <g:radioGroup name="knownStroke" labels="['Yes','No']" values="['Y','N']" required="true">
                                                    <div class="radio col-sm-3">
                                                        <p>${it.label} ${it.radio}</p>
                                                    </div>
                                                </g:radioGroup>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3 col-sm-offset-9">
                                                <g:submitButton name="Add" value="Add Member" class="btn btn-sm btn-block btn-success"/>
                                            </div>
                                        </div>
                                    </g:form>
                                </div>
                            </div>
                            <!-- /panel-body -->
                        </div>
                        <!-- /panel-footer -->
                    </div>
                </div>
                <!-- /panel-container div -->
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 hidden-xs hidden-sm">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Family Details
                            </h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 hidden-xs hidden-sm" align="center">
                                    <g:img dir="images" file="family.png" alt="family" width="96" height="96" class="img-circle"/>
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <div class="table-responsive">
                                        <table class="table table-user-information">
                                            <tbody>
                                                <tr>
                                                    <td>Family ID:</td>
                                                    <td>${family?.assignedId}</td>
                                                </tr>
                                                <tr>
                                                    <td>Head of Family:</td>
                                                    <td>
                                                        <g:each in="${family?.persons}" var="member">
                                                            <g:if test="${member.class.name == 'org.jnanasanjeevini.FamilyHead'}">
                                                                ${member.firstName} ${member.lastName}
                                                            </g:if>
                                                        </g:each>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Annual Family Income:</td>
                                                    <td>${family?.annualFamilyIncome}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address:</td>
                                                    <td>${family?.address}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location Type:</td>
                                                    <td>${family?.locationType}</td>
                                                </tr>
                                                <tr>
                                                    <td>Landline Number:</td>
                                                    <td>${family?.landlineNumber}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /panel-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- /panel-container div -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Members of this Family <span class="badge badge-default pull-right">${family?.persons?.size()}</span></h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <g:each in="${family?.persons?.sort()}" var="member">
                                        <g:link action="showFamilyMember" id="${member.id}" class="btn btn-xs">
                                            ${member?.firstName} ${member?.lastName }
                                        </g:link>
                                    </g:each>
                                </div>
                            </div>
                            <!-- /panel-body -->
                        </div>
                        <!-- /panel-container div -->
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>