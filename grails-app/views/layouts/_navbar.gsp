<nav class="navbar navbar-fixed-top navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <g:link controller="family" action="myFamilies" class="navbar-brand">
        <g:img dir="images" file="logo.png" alt="Peers For Health"/>
      </g:link>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <sec:ifLoggedIn>
          <li><g:link controller="family" action="myFamilies">All Families</g:link></li>
            <li><g:link controller="family" action="addFamily">Add Family</g:link></li>
            <li><g:link controller="logout" action="index">Logout</g:link></li>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <li><g:link controller='login' action='auth'>Login</g:link></li>
        </sec:ifNotLoggedIn>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>