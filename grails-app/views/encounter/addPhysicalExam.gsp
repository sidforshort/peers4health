<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Add Physical Examination Details | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <g:hasErrors bean="${physicalExam}">
	                    <g:eachError>
	                        <p>
	                            <g:message error="${it}"/>
	                        </p>
	                    </g:eachError>
	                </g:hasErrors>
	            </div>
	        </div>
	        
	        <div class="row">
		        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Physical Attributes</h3>
	                        <!-- <div class="pull-right">
	                            <span class="label label-danger">BMI: 128</span>
	                            <span class="label label-info">WHR: 128</span>
	                        </div> -->
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="physical.png" alt="physical exam" width="96" height="96" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                                <g:form action="addPhysicalExam" class="form-horizontal" role="form">
	                                    <g:hiddenField name="encounterId" value="${encounterId}" />
	                                    <div class="form-group">
	                                        <label for="heightInCms" class="col-sm-3 control-label">Height</label>
	                                        <div class="col-sm-9">
	                                            <g:textField name="heightInCms" value="${physicalExam?.heightInCms}" class="form-control" placeholder="value in Cms." type="number" required="true" />
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="weightInKgs" class="col-sm-3 control-label">Weight</label>
	                                        <div class="col-sm-9">
	                                            <g:textField name="weightInKgs" value="${physicalExam?.weightInKgs}" class="form-control" placeholder="value in Kgs." type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="waistInCms" class="col-sm-3 control-label">Waist Size</label>
	                                        <div class="col-sm-9">
	                                            <g:textField name="waistInCms" value="${physicalExam?.waistInCms}" class="form-control" placeholder="value in Cms." type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="hipInCms" class="col-sm-3 control-label">Hip Size</label>
	                                        <div class="col-sm-9">
	                                            <g:textField name="hipInCms" value="${physicalExam?.hipInCms}" class="form-control" placeholder="value in Cms." type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="comments" class="col-sm-3 control-label">Comments</label>
	                                        <div class="col-sm-9">
	                                            <g:textArea name="comments" cols="6" class="form-control" placeholder="Add your comments here (optional)" value="${physicalExam?.comments}"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <div class="col-sm-4 col-sm-offset-8">
	                                            <g:submitButton name="Add" value="Next" class="btn btn-sm btn-block btn-primary"/>
	                                        </div>
	                                    </div>
	                                </g:form>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	            </div>
	            <!-- /panel container div -->
		    </div>
	    </div>
    </body>
</html>