<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Add Outcome/Follow-up Details | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <g:hasErrors bean="${screeningOutcome}">
	                    <g:eachError>
	                        <p>
	                            <g:message error="${it}"/>
	                        </p>
	                    </g:eachError>
	                </g:hasErrors>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Outcome/Follow-Up</h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
                                    <g:img dir="images" file="outcome.png" alt="outcome" width="96" height="96" class="img-circle"/>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                    <g:form action="addOutcome" class="form-horizontal" role="form">
                                        <g:hiddenField name="encounterId" value="${encounterId}" />
                                        <div class="form-group">
                                            <label for="dmTreatment" class="col-sm-6 control-label">Diabetes Treatment</label>
                                            <div class="col-sm-6">
                                                <g:select name="dmTreatment" from="['None', 'Tablets', 'Insulin']" noSelection="['':'- Choose Treatment -']" class="form-control" value="${screeningOutcome?.dmTreatment}" required="true" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="htTreatment" class="col-sm-6 control-label">Hypertension Treatment</label>
                                            <div class="col-sm-6">
                                                <g:select name="htTreatment" from="['None', 'Tablets']" noSelection="['':'- Choose Treatment -']" value="${screeningOutcome?.htTreatment}" class="form-control" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="screeningAction" class="col-sm-6 control-label">Screening Action</label>
                                            <div class="col-sm-6">
                                                <g:select name="screeningAction" from="['Normal: health counselling', 'Borderline Diabetes: health counselling', 'Borderline Hypertension: health counselling', 'Diabetes: refer medical doctor', 'Hypertension: refer medical doctor', 'Urgent medical care', 'Emergency hospitalization', 'Follow-up with private doctor', 'Follow-up with Govt. hospital', 'Follow-up with JS', 'Other']" noSelection="['':'- Screening Action -']" value="${screeningOutcome?.screeningAction}" class="form-control" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nextAppointment" class="col-sm-6 control-label">Next Appointment</label>
                                            <div class="col-sm-6">
                                                <g:select name="nextAppointment" from="['Confirmation', 'Monthly Medical Review', 'Quarterly Medical Review', 'Annual Medical Review', 'Hospital Admission', 'Annual Rescreening', 'Other']" noSelection="['':'- Next Appointment -']" value="${screeningOutcome?.nextAppointment}" class="form-control" required="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="comments" class="col-sm-6 control-label">Observations</label>
                                            <div class="col-sm-6">
                                                <g:textArea name="comments" cols="6" class="form-control" placeholder="Add your comments here (optional)" value="${screeningOutcome?.comments}"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-8">
                                                <g:submitButton name="Add" value="Next" class="btn btn-sm btn-block btn-primary"/>
                                            </div>
                                        </div>
                                    </g:form>
                                </div>
                            </div>
                            <!-- /panel-body -->
                        </div>
                        <!-- /panel -->
                    </div>
                    <!-- /panel container div -->
                </div>
	        </div>
        </div>
    </body>
</html>