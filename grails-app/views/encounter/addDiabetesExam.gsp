<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Blood Glucose & Diabetes Screening | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <g:hasErrors bean="${diabetesExam}">
	                    <g:eachError>
	                        <p>
	                            <g:message error="${it}"/>
	                        </p>
	                    </g:eachError>
	                </g:hasErrors>
	            </div>
	        </div>
	        
	        <div class="row">
		        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Blood Glucose & Diabetes Screening</h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="diabetes.png" alt="diabetes test" width="96" height="96" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                                <g:form action="addDiabetesExam" class="form-horizontal" role="form">
	                                    <g:hiddenField name="encounterId" value="${encounterId}" />
	                                    <div class="form-group">
	                                        <label for="fastingReading" class="col-sm-6 control-label">Blood Glucose Fasting</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="fastingReading" value="${diabetesExam?.fastingReading}" class="form-control" placeholder="Reading value" type="number" required="true" />
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="pm2hReading" class="col-sm-6 control-label">Blood Glucose Post Meal (2hrs)</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="pm2hReading" value="${diabetesExam?.pm2hReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="randomReading" class="col-sm-6 control-label">Blood Glucose Random</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="randomReading" value="${diabetesExam?.randomReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="normalReading" class="col-sm-6 control-label">Normal</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="normalReading" value="${diabetesExam?.normalReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="iftReading" class="col-sm-6 control-label">Borderline Diabetes [IFT]</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="iftReading" value="${diabetesExam?.iftReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="igtReading" class="col-sm-6 control-label">Borderline Diabetes [IGT]</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="igtReading" value="${diabetesExam?.igtReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="dmReading" class="col-sm-6 control-label">Diabetes Melitus</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="dmReading" value="${diabetesExam?.dmReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <div class="col-sm-4 col-sm-offset-8">
	                                            <g:submitButton name="Next" value="Next" class="btn btn-sm btn-block btn-primary"/>
	                                        </div>
	                                    </div>
	                                </g:form>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	                <!-- /panel container div -->
	            </div>
	        </div>
        </div>
    </body>
</html>