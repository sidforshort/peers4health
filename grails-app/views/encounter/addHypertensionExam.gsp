<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Blood Pressure & Hypertension Screening | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <g:hasErrors bean="${hypertensionExam}">
	                    <g:eachError>
	                        <p>
	                            <g:message error="${it}"/>
	                        </p>
	                    </g:eachError>
	                </g:hasErrors>
	            </div>
	        </div>

	        <div class="row">
		        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Blood Pressure & Hypertension Screening</h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="hypertension.png" alt="hypertension exam" width="96" height="96" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                                <g:form action="addHypertensionExam" class="form-horizontal" role="form">
	                                    <g:hiddenField name="encounterId" value="${encounterId}" />
	                                    <div class="form-group">
	                                        <label for="systolic1A" class="col-sm-6 control-label">Blood Pressure Systolic 1A</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="systolic1A" value="${hypertensionExam?.systolic1A}" class="form-control" placeholder="Reading value" type="number" required="true" />
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="diastolic1A" class="col-sm-6 control-label">Blood Pressure Diastolic 1A</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="diastolic1A" value="${hypertensionExam?.diastolic1A}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="pulseRate1A" class="col-sm-6 control-label">Pulse Rate 1A</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="pulseRate1A" value="${hypertensionExam?.pulseRate1A}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="systolic1B" class="col-sm-6 control-label">Blood Pressure Systolic 1B</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="systolic1B" value="${hypertensionExam?.systolic1B}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="diastolic1B" class="col-sm-6 control-label">Blood Pressure Diastolic 1B</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="diastolic1B" value="${hypertensionExam?.diastolic1B}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="pulseRate1B" class="col-sm-6 control-label">Pulse Rate 1B</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="pulseRate1B" value="${hypertensionExam?.pulseRate1B}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="normalReading" class="col-sm-6 control-label">Normal Hypertension</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="normalReading" value="${hypertensionExam?.normalReading}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="preHypertension" class="col-sm-6 control-label">Pre Hypertension</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="preHypertension" value="${hypertensionExam?.preHypertension}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="hypertension" class="col-sm-6 control-label">Hypertension</label>
	                                        <div class="col-sm-6">
	                                            <g:textField name="hypertension" value="${hypertensionExam?.hypertension}" class="form-control" placeholder="Reading value" type="number" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <div class="col-sm-4 col-sm-offset-8">
	                                            <g:submitButton name="Add" value="Next" class="btn btn-sm btn-block btn-primary"/>
	                                        </div>
	                                    </div>
	                                </g:form>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	            </div>
	            <!-- /panel container div -->
	        </div>
        </div>
    </body>
</html>