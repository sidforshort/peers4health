<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Viewing Encounter Details | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
	        <%
	        	def diabetesColorCode = ""
	        	switch (encounter?.screeningOutcome?.diabetesPrognosis) {
				    case "Normal":
				        diabetesColorCode = "success"
				        break

				    case ["Diabetic", "Unsure"]:
				        diabetesColorCode = "danger"
				        break

				    case "Borederline Diabetic":
				        diabetesColorCode = "warning"
				        break

				    default:
				        diabetesColorCode = "default"
				}

				def hypertensionColorCode = ""
	        	switch (encounter?.screeningOutcome?.hypertensionPrognosis) {
				    case ["Low Normal", "Prehypertension"]:
				        hypertensionColorCode = "warning"
				        break

				    case "Normal":
				        hypertensionColorCode = "success"
				        break

				    case ["Dangerously Low", "Low", "High - Stage 1", "High - Stage 2", "High - Stage 3", "High - Stage 4", "Unsure"]:
				        hypertensionColorCode = "danger"
				        break

				    default:
				        hypertensionColorCode = "default"
				}
	    	%>

	        <div class="row">
	        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Outcome/Follow-Up <span class="badge pull-right">${encounter?.person?.firstName} ${encounter?.person?.lastName}</span> </h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="outcome.png" alt="prognosis" width="192" height="192" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                            	<div class="table-responsive">
		                            	<table class="table table-user-information">
						                    <tbody>
						                    	<tr>
						                        	<td>Diabetes Prognosis:</td>
						                        	<td>${encounter?.screeningOutcome?.diabetesPrognosis}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Diabetes Treatment:</td>
						                        	<td>${encounter?.screeningOutcome?.dmTreatment}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Hypertension Prognosis:</td>
						                        	<td>${encounter?.screeningOutcome?.hypertensionPrognosis}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Hypertension Treatment:</td>
						                        	<td>${encounter?.screeningOutcome?.htTreatment}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Screening Action:</td>
						                        	<td>${encounter?.screeningOutcome?.screeningAction}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Next Appointment:</td>
						                        	<td>${encounter?.screeningOutcome?.nextAppointment}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Observations:</td>
						                        	<td>${encounter?.screeningOutcome?.comments}</td>
						                      	</tr>
						                    </tbody>
						                </table>
					                </div>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	            </div>
	            <!-- /screening outcome -->
	        </div>

			<div class="row">
	            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 hidden-xs hidden-sm">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Physical Attributes</h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                        	<div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="physical.png" alt="physical" width="48" height="48" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                            	<div class="table-responsive">
		                            	<table class="table table-user-information">
						                    <tbody>
						                      	<tr>
						                        	<td>Height in Cms:</td>
						                        	<td>${encounter?.physicalExam?.heightInCms}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Weight in Kgs:</td>
						                        	<td>${encounter?.physicalExam?.weightInKgs}</td>
						                      	</tr>
						                      	<%
						                      		def heightInMeters = encounter?.physicalExam?.heightInCms/100
						                      		def bmi = (encounter?.physicalExam?.weightInKgs)/(heightInMeters*heightInMeters)
						                      		def cssClass = "default"
											        switch (bmi) {
											            case [0.0..18.5]:
											                bmiPrognosis = "Underweight"
											                cssClass = "danger"
											                break
											            case [18.6..24.9]:
											                bmiPrognosis = "Normal"
											                cssClass = "success"
											                break
											            case [25.0..29.9]:
											                bmiPrognosis = "Overweight"
											                cssClass = "warning"
											                break
											            case [30.0..50]:
											                bmiPrognosis = "Obese"
											                cssClass = "danger"
											                break
											            default:
											                bmiPrognosis = "Normal"
											                cssClass = "success"
											        }
						                      	%>
						                      	<tr>
						                        	<td>BMI:</td>
						                        	<td>
						                        		<g:formatNumber number="${bmi}" type="number" maxFractionDigits="2" />
						                        		<span class="label label-${cssClass} pull-right">${bmiPrognosis}</span>
						                        	</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Waist in Cms:</td>
						                        	<td>${encounter?.physicalExam?.waistInCms}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Hip in Cms:</td>
						                        	<td>${encounter?.physicalExam?.hipInCms}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>WHR:</td>
						                        	<g:set var="whrValue" value="${(encounter?.physicalExam?.waistInCms)/(encounter?.physicalExam?.hipInCms)}" />
						                        	<td><g:formatNumber number="${whrValue}" type="number" maxFractionDigits="2" /></td>
						                      	</tr>
						                    </tbody>
						                </table>
					                </div>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	            </div>
	            <!-- /physical attributes -->

	            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 hidden-xs hidden-sm">

	                <div class="panel panel-${diabetesColorCode}">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Blood Sugar & Diabetes Screening</h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="diabetes.png" alt="physical" width="48" height="48" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                            	<div class="table-responsive">
		                            	<table class="table table-user-information">
						                    <tbody>
						                      	<tr>
						                        	<td>Blood Glucose Fasting:</td>
						                        	<td>${encounter?.diabetesExam?.fastingReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Blood Glucose Post Meal (2hrs):</td>
						                        	<td>${encounter?.diabetesExam?.pm2hReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Blood Glucose Random:</td>
						                        	<td>${encounter?.diabetesExam?.randomReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Normal:</td>
						                        	<td>${encounter?.diabetesExam?.normalReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Borderline Diabetes [IFT]:</td>
						                        	<td>${encounter?.diabetesExam?.iftReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Borderline Diabetes [IGT]:</td>
						                        	<td>${encounter?.diabetesExam?.igtReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Diabetes Melitus:</td>
						                        	<td>${encounter?.diabetesExam?.dmReading}</td>
						                      	</tr>
						                    </tbody>
						                </table>
					                </div>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	            </div>
	            <!-- /diabetes & bg attributes -->

	            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 hidden-xs hidden-sm">
	                <div class="panel panel-${hypertensionColorCode}">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Blood Pressure & Hypertension Screening</h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="hypertension.png" alt="physical" width="48" height="48" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                            	<div class="table-responsive">
		                            	<table class="table table-user-information">
						                    <tbody>
						                      	<tr>
						                        	<td>Blood Pressure Systolic 1A:</td>
						                        	<td>${encounter?.hypertensionExam?.systolic1A}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Blood Pressure Diastolic 1A:</td>
						                        	<td>${encounter?.hypertensionExam?.diastolic1A}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Pulse Rate 1A:</td>
						                        	<td>${encounter?.hypertensionExam?.pulseRate1A}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Blood Pressure Systolic 1B:</td>
						                        	<td>${encounter?.hypertensionExam?.systolic1B}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Blood Pressure Diastolic 1B:</td>
						                        	<td>${encounter?.hypertensionExam?.diastolic1B}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Pulse Rate 1B:</td>
						                        	<td>${encounter?.hypertensionExam?.pulseRate1B}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Normal Hypertension:</td>
						                        	<td>${encounter?.hypertensionExam?.normalReading}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Pre Hypertension:</td>
						                        	<td>${encounter?.hypertensionExam?.preHypertension}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Hypertension:</td>
						                        	<td>${encounter?.hypertensionExam?.hypertension}</td>
						                      	</tr>
						                    </tbody>
						                </table>
					                </div>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel -->
	                </div>
	            </div>
	            <!-- /hypertension & bp attributes -->
	        </div>

	    </div>
	    
    </body>
</html>