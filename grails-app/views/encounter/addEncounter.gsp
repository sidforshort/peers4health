<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="layout" content="main"/>
		<title>Add Encounter Details | Peers for Health</title>
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
	</head>
	<body>
		<div class="container-fluid">
            <div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<g:hasErrors bean="${encounter}">
						<g:eachError>
							<p>
								<g:message error="${it}"/>
							</p>
						</g:eachError>
					</g:hasErrors>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
					<g:form action="addEncounter" class="form-horizontal" role="form">
						<g:hiddenField name="personId" value="${params?.personId}" />
						<div class="form-group">
							<label for="encounterType" class="col-sm-3 control-label">Type of Encounter</label>
							<div class="col-sm-6">
								<g:select name="encounterType" from="['Initial Screening', 'Confirmation', 'Annual Rescreening', 'Review']" noSelection="['':'- Pick An Option -']" class="form-control" />
							</div>
							<div class="col-sm-3 pull-right">
								<g:submitButton name="Add" value="Next" class="btn btn-md btn-primary"/>
							</div>
						</div>
					</g:form>
				</div>
			</div>

		</div>
	</body>
</html>