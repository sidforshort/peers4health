<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Viewing Encounters Per Person | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Past Encounters for ${person?.firstName} <span class="label label-default pull-right">${person?.assignedId}</span></h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>More Details</th>
                                                <th>Encounter Type</th>
                                                <th>Encounter Date</th>
                                                <th>Diabetes Treatment</th>
                                                <th>Hypertension Treatment</th>
                                                <th>Screening Action</th>
                                                <th>Next Appointment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${encounters}" var="encounter">
                                                <tr>
                                                    <g:link controller="encounter" action="viewEncounter" id="${encounter?.id}" class="btn btn-xs btn-default"> <span class="glyphicon glyphicon-th-list"></span> View Encounter</g:link>
                                                    <td>${encounter?.encounterType}</td>
                                                    <td><g:formatDate format="dd MMM, yyyy" date="${encounter?.dateCreated}"/></td>
                                                    <td>${encounter?.screeningOutcome?.dmTreatment}</td>
                                                    <td>${encounter?.screeningOutcome?.htTreatment}</td>
                                                    <td>${encounter?.screeningOutcome?.screeningAction}</td>
                                                    <td>${encounter?.screeningOutcome?.nextAppointment}</td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /panel-body -->
                        </div>
                        <!-- /panel-footer -->
                    </div>
                </div>
                <!-- /panel-container div -->
            </div>
        </div>
    </body>
</html>