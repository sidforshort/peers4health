<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="layout" content="main"/>
    <title>Thanks for Confirming your Sign-up! | Peers for Health</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'outcomes.css')}" type="text/css">
  </head>
  <body>
    <section style="background:#efefe9;">
      <div class="container">
        <div class="row">
          <div class="board">
            <div class="board-inner">
              <ul class="nav nav-tabs" id="myTab">
                <div class="liner"></div>
                <li class="active">
                  <a href="#registerChw" data-toggle="tab" title="Register CHW">
                  <span class="round-tabs one">
                  <i class="glyphicon glyphicon-home"></i>
                  </span> 
                  </a>
                </li>
                <li><a href="#addFamily" data-toggle="tab" title="Add Family">
                  <span class="round-tabs two">
                  <i class="glyphicon glyphicon-user"></i>
                  </span> 
                  </a>
                </li>
                <li><a href="#addFamilyMembers" data-toggle="tab" title="Add Family Members">
                  <span class="round-tabs three">
                  <i class="glyphicon glyphicon-gift"></i>
                  </span> </a>
                </li>
                <li><a href="#addEncounter" data-toggle="tab" title="Add Encounter">
                  <span class="round-tabs four">
                  <i class="glyphicon glyphicon-comment"></i>
                  </span> 
                  </a>
                </li>
                <li><a href="#viewOutcome" data-toggle="tab" title="Completed">
                  <span class="round-tabs five">
                  <i class="glyphicon glyphicon-ok"></i>
                  </span> </a>
                </li>
              </ul>
            </div>

            <div class="tab-content">
              <div class="tab-pane fade in active" id="registerChw">
                <h3 class="head text-center">Register a CHW</h3>
                <p class="text-center">
                  <g:link controller="user" action="register" class="btn btn-success btn-outline-rounded green"> Start <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></g:link>
                </p>
              </div>  <!-- /registerChw -->

              <div class="tab-pane fade" id="addFamily">
                <h3 class="head text-center">Add Family & Head of Family</h3>
                <p class="text-center">
                  <g:link controller="family" action="addFamily" class="btn btn-success btn-outline-rounded green"> Start <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></g:link>
                </p>
              </div>  <!-- /addFamily -->

              <div class="tab-pane fade" id="addFamilyMembers">
                <h3 class="head text-center">Add Family Members</h3>
                <p class="text-center">
                  <g:link controller="person" action="addFamilyMembers" class="btn btn-success btn-outline-rounded green"> Start <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></g:link>
                </p>
              </div>  <!-- /addFamily -->

              <div class="tab-pane fade" id="addEncounter">
                <h3 class="head text-center">Add Encounter</h3>
                <p class="text-center">
                  <g:link controller="encounter" action="addEncounter" class="btn btn-success btn-outline-rounded green"> Start <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></g:link>
                </p>
              </div>  <!-- /addEncounter -->

              <div class="tab-pane fade" id="viewOutcome">
                <div class="text-center">
                  <i class="img-intro icon-checkmark-circle"></i>
                </div>
                <h3 class="head text-center">View Outcomes</h3>
                <p class="text-center">
                  <g:link controller="screeningOutcome" action="showOutcome" class="btn btn-success btn-outline-rounded green"> Start <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></g:link>
                </p>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <g:javascript>
      $(function(){
        $('a[title]').tooltip();
      });
    </g:javascript>
  </body>
</html>