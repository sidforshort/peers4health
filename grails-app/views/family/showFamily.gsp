<%@ page import="org.jnanasanjeevini.*" %>
<%@ page import="org.joda.time.Period" %>
<%@ page import="org.joda.time.DateTime" %>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="layout" content="main"/>
		<title>Show Family Details | Peers for Health</title>
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">	
			        <div class="panel panel-default">
			            <div class="panel-heading">
			            	<h3 class="panel-title">
			            		Family Details
								<span class="pull-right">
									<g:link class="btn btn-success btn-xs" controller="person" action="addFamilyMember" params="[familyId:"${family?.id}"]"><span class="glyphicon glyphicon-plus-sign"></span> Add Member</g:link>
		                        </span>
			            	</h3>
			            </div> <!-- /panel-heading -->

			            <div class="panel-body">
			              	<div class="row">
				                <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
				                	<g:img dir="images" file="family.png" alt="family" width="96" height="96" class="img-circle"/>
				                </div>
				                
				                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				                	<div class="table-responsive">
					                  	<table class="table table-user-information">
						                    <tbody>
						                      	<tr>
						                        	<td>Family ID:</td>
						                        	<td>${family?.assignedId}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Annual Family Income:</td>
						                        	<td>${family?.annualFamilyIncome}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Address:</td>
						                        	<td>${family?.address}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Location Type:</td>
						                        	<td>${family?.locationType}</td>
						                      	</tr>
						                      	<tr>
						                        	<td>Landline Number:</td>
						                        	<td>${family?.landlineNumber}</td>
						                      	</tr>				                     
						                    </tbody>
						                </table>
					                </div>
				        		</div>
				            <div>
			          	</div>
			          	
			        </div> <!-- /panel-container div -->
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3>Members of this Family</h3>
					<div class="table-responsive">
		             	<table id="mytable" class="table table-bordred table-striped">
		                   	<thead>
		                   		<th>Full Name</th>
		                   		<th>ID</th>
								<th>Gender</th>
								<th>Age</th>
								<th>Mobile No.</th>
								<th>Enrolment Date</th>
								<th>Enrolled By</th>
								<th>View Encounters</th>
								<th>Add Encounter</th>
							</thead>
							<tbody>
								<g:each in="${family?.persons}" var="person">
									<%
										def now = new DateTime()
										def yob = new DateTime(person?.yearOfBirth)
										def period = new Period(yob, now)
									%>
									<tr>
										<td>
											${person?.firstName} ${person?.lastName }
											<g:if test="${person.class.name == 'org.jnanasanjeevini.FamilyHead'}">
											    <span class="label label-primary">Head</span>
											</g:if>
										</td>
										<td>${person?.assignedId}</td>
										<td>${person?.gender}</td>
										<td>${period?.years}</td>
									    <td>${person?.mobileNumber}</td>
									    <td><g:formatDate date="${person?.dateEnrolled}" format="d MMM, yyyy"/></td>
									    <td>${person?.family?.user?.profile?.firstName}</td>
									    <td><p><g:link class="btn btn-default btn-xs btn-block" controller="encounter" action="showEncounters" params="[personId:"${person?.id}"]"><span class="glyphicon glyphicon-eye-open"></span> View</g:link></p></td>
										<td><p><g:link class="btn btn-default btn-xs btn-block" controller="encounter" action="addEncounter" params="[personId:"${person?.id}"]"><span class="glyphicon glyphicon-plus-sign"></span> Add</g:link></p></td>
									</tr>
								</g:each>
							<tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>