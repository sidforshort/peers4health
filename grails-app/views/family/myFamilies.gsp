<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Viewing Registered Family Details | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">My Registered Families <span class="label label-default pull-right">${user?.profile?.assignedId}</span></h3>
                        </div>
                        <!-- /panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Add Person to Family</th>
                                                <th>Add Encounter</th>
                                                <th>Family ID</th>
                                                <th>Family Members</th>
                                                <th>Address</th>
                                                <th>Phone</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${families}" var="family">
                                                <tr>
                                                    <td>
                                                        <g:link controller="person" action="addFamilyMember" params="[familyId:"${family?.id}"]" class="btn btn-xs btn-default"> <span class="glyphicon glyphicon-plus"></span> Add Person</g:link>
                                                    </td>
                                                    <g:each in="${family?.persons?.sort()}" var="member">
                                                        <g:if test="${member.class.name == 'org.jnanasanjeevini.FamilyHead'}">
                                                            <g:set var="headOfFamily" value="${member}"/>
                                                        </g:if>
                                                    </g:each>
                                                    <td>
                                                        <g:link controller="encounter" action="addEncounter" params="[personId:"${headOfFamily?.id}"]" class="btn btn-xs btn-default"> <span class="glyphicon glyphicon-th-list"></span> Add Encounter</g:link>
                                                    </td>
                                                    <td>${family?.assignedId}</td>
                                                    <td>
                                                        <g:each in="${family?.persons?.sort()}" var="member">
                                                            <g:link controller="person" action="showFamilyMember" id="${member.id}" class="btn btn-xs">
                                                                ${member?.firstName} ${member?.lastName }
                                                            </g:link>
                                                        </g:each>
                                                    </td>
                                                    <td>${family?.address}</td>
                                                    <td>${family?.landlineNumber}</td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /panel-body -->
                        </div>
                        <!-- /panel-footer -->
                    </div>
                </div>
                <!-- /panel-container div -->
            </div>
        </div>
    </body>
</html>