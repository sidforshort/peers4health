<%@ page import="org.jnanasanjeevini.*" %>
<%@ page import="org.joda.time.Period" %>
<%@ page import="org.joda.time.DateTime" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Add New Family & Head of Family Details | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
	        <div class="row">
		        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		            <g:hasErrors bean="${familyHead}">
		                <g:eachError>
		                    <p>
		                        <g:message error="${it}"/>
		                    </p>
		                </g:eachError>
		            </g:hasErrors>
		        </div>
		    </div>

		    <div class="row">
		        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">New Family Details <span class="label label-default pull-right">${user?.profile?.assignedId}</span></h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                        	<div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="family.png" alt="family" width="96" height="96" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                                <g:form action="addFamily" class="form-horizontal" role="form">
	                                    <g:hiddenField name="userId" value="${user?.id}" />
	                                    <div class="form-group">
	                                        <label for="firstName" class="col-sm-4 control-label">Name</label>
	                                        <div class="col-sm-4">
	                                            <g:textField name="firstName" value="${familyHead?.firstName}" class="form-control" placeholder="First name" required="true" />
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <g:textField name="lastName" value="${familyHead?.lastName}" class="form-control" placeholder="Last name" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="dateEnrolled" class="col-sm-4 control-label">Date Enrolled</label>
	                                        <div class="col-sm-8">
	                                            <g:datePicker name="dateEnrolled" class="form-control" value="${familyHead?.dateEnrolled}" precision="day" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="consentForScreening" class="col-sm-4 control-label">Consent For Screening</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="consentForScreening" labels="['Yes','No']" values="['Y','N']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <hr>
	                                    <div class="form-group">
	                                        <label for="numberOfChildren" class="col-sm-4 control-label">Number of Children</label>
	                                        <div class="col-sm-4">
	                                            <g:textField name="numberOfChildren" value="${familyHead?.numberOfChildren}" class="form-control" placeholder="Number of Children" type="number" min="0" step="1" required="true"/>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <g:textField name="numberOfGrandChildren" value="${familyHead?.numberOfGrandChildren}" class="form-control" placeholder="Number of Grand Children" type="number" min="0" step="1" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="yearOfBirth" class="col-sm-4 control-label">Year of birth</label>
	                                        <div class="col-sm-8">
	                                            <g:datePicker name="yearOfBirth" class="form-control" value="${familyHead?.yearOfBirth}" precision="year" placeholder="Year of Birth" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="aadharID" class="col-sm-4 control-label">Aadhar ID</label>
	                                        <div class="col-sm-8">
	                                            <g:textField name="aadharID" value="${familyHead?.aadharID}" class="form-control" placeholder="Aadhar ID" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="educationLevel" class="col-sm-4 control-label">Education</label>
	                                        <div class="col-sm-8">
	                                            <g:select name="educationLevel" from="['None', 'Primary Education', 'High School', 'Graduate', 'Post Graduate', 'Vocational Training']" noSelection="['':'- Choose Education -']" class="form-control" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="annualFamilyIncome" class="col-sm-4 control-label">Family Income</label>
	                                        <div class="col-sm-8">
	                                            <g:select name="annualFamilyIncome" from="['Less than ₹ 50,000', '₹ 50,001 - ₹ 1,00,000', '₹ 1,00,001 - ₹ 5,00,000', 'More than ₹ 5,00,000']" noSelection="['':'- Pick Family Income -']" class="form-control" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="address" class="col-sm-4 control-label">Address</label>
	                                        <div class="col-sm-8">
	                                            <g:textArea name="address" rows="3" class="form-control" placeholder="Residential address" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="locationType" class="col-sm-4 control-label">Location</label>
	                                        <div class="col-sm-8">
	                                            <g:select name="locationType" from="['Urban', 'Semi Urban', 'Rural']" noSelection="['':'- Choose Location Type -']" class="form-control" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="mobileNumber" class="col-sm-4 control-label">Contact Information</label>
	                                        <div class="col-sm-4">
	                                            <g:textField name="mobileNumber" value="${familyHead?.mobileNumber}" class="form-control" placeholder="Mobile number" type="tel" required="true"/>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <g:textField name="landlineNumber" class="form-control" placeholder="Landline number" type="tel" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="gender" class="col-sm-4 control-label">Gender</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="gender" labels="['Male','Female']" values="['M','F']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="knownDiabetes" class="col-sm-4 control-label">Known Diabetes?</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="knownDiabetes" labels="['Yes','No']" values="['Y','N']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="knownHypertension" class="col-sm-4 control-label">Hypertension?</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="knownHypertension" labels="['Yes','No']" values="['Y','N']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="knownHeartDisease" class="col-sm-4 control-label">Heart Disease?</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="knownHeartDisease" labels="['Yes','No']" values="['Y','N']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="knownStroke" class="col-sm-4 control-label">Known Stroke?</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="knownStroke" labels="['Yes','No']" values="['Y','N']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="spouseAlive" class="col-sm-4 control-label">Is Spouse Alive?</label>
	                                        <div class="col-sm-8">
	                                            <g:radioGroup name="spouseAlive" labels="['Yes','No']" values="['Y','N']" required="true">
	                                                <div class="radio col-sm-3">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <div class="col-sm-3 col-sm-offset-9">
	                                            <g:submitButton name="Add" value="Add Family" class="btn btn-sm btn-block btn-success"/>
	                                        </div>
	                                    </div>
	                                </g:form>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel-footer -->
	                </div>
	            </div>
	            <!-- /panel-container div -->
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 hidden-xs hidden-sm">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">
	                            CHW For This Family
	                            <span class="pull-right label label-default">${user?.profile?.assignedId}</span>
	                        </h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-2 col-lg-2 hidden-xs hidden-sm" align="center">
	                                <g:img dir="images" file="chw.png" alt="CHW" width="64" height="64" class="img-circle"/>
	                            </div>
	                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
	                            	<div class="table-responsive">
		                                <table class="table table-user-information">
		                                    <tbody>
		                                        <tr>
		                                            <td>CHW ID:</td>
		                                            <td>${user?.profile?.assignedId}</td>
		                                        </tr>
		                                        <tr>
		                                            <td>Full Name:</td>
		                                            <td>${user?.profile?.firstName} ${user?.profile?.lastName}</td>
		                                        </tr>
		                                        <tr>
		                                            <td>Gender:</td>
		                                            <td>${user?.profile?.gender}</td>
		                                        </tr>
		                                        <tr>
		                                            <td>Address:</td>
		                                            <td>${user?.profile?.address}</td>
		                                        </tr>
		                                        <tr>
		                                            <td>Mobile Phone:</td>
		                                            <td>${user?.profile?.mobileNumber}</td>
		                                        </tr>
		                                    </tbody>
		                                </table>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- /panel-body -->
	                </div>
	            </div>
	            <!-- /panel-container div -->
	        </div>
        </div>
    </body>
</html>