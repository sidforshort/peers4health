<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name='layout' content='no_nav'/>
    <title><g:message code="springSecurity.login.title"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'login.css')}" type="text/css">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-4">
                <h2 class="text-center">P4H Sign-in</h2>
                <div class="account-wall">
                    <g:img dir="images" file="person.png" class="profile-img" width="120" height="120"/>
                    <g:if test="${flash.message}">
                        <div class="alert alert-danger">${flash.message}</div>
                    </g:if>
                    <br>
                    <form class="form-signin" action='${postUrl}' method='POST' id='loginForm' autocomplete='off'>
                        <input type="text" class="form-control" placeholder="Email" name='j_username' id='username' required autofocus>
                        <input type="password" class="form-control" placeholder="Password" name='j_password' id='password' required>
                        <button class="btn btn-lg btn-info btn-block" type="submit">
                            Sign in</button>
                        <label class="checkbox pull-left">
                            <input type="checkbox" value="remember-me" id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                            Remember me
                        </label>
                        <a href="#" class="pull-right need-help">Need help?</a><span class="clearfix"></span>
                    </form>
                </div>
                <a href="${createLink(controller:"user", action:"register")}" class="text-center new-account"> Register as a Community Health Worker</a>
            </div>
        </div>
    </div>
    <script src="js/bootstrap.min.js"></script>
    <script type='text/javascript'>
        <!--
        (function() {
            document.forms['loginForm'].elements['j_username'].focus();
        })();
        // -->
    </script>
  </body>
</html>