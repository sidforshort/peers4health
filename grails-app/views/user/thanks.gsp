<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="no_nav"/>
        <title>Thanks for Signing-up! | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'outcomes.css')}" type="text/css">
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container-fluid">
                <div class="row">
                    <div class="board">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="thanks">
                                <h3 class="head text-center">Thanks for Signing-up <span style="color:#f48260;">♥</span></h3>
                                <p class="narrow text-center">
                                    Check your email for instructions to complete the sign-up & start using Peers for Health!
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>