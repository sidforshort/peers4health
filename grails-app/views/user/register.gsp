<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main"/>
        <title>Sign-up for Peers for Health | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'showFamily.css')}" type="text/css">
    </head>
    <body>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
		            <g:hasErrors bean="${user}">
		                <g:eachError>
		                    <p>
		                        <g:message error="${it}"/>
		                    </p>
		                </g:eachError>
		            </g:hasErrors>
		        </div>
        	</div>

	        <div class="row">
		        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="panel-title">Sign-up as a Community Health Worker</h3>
	                    </div>
	                    <!-- /panel-heading -->
	                    <div class="panel-body">
	                        <div class="row">
	                            <div class="col-md-12 col-lg-12">
	                                <g:form action="register" class="form-horizontal" role="form">
	                                    <div class="form-group">
	                                        <label for="email" class="col-sm-3 control-label">Email ID</label>
	                                        <div class="col-sm-9">
	                                            <g:textField name="email" value="${user?.email}" class="form-control" placeholder="Email (will be your username)" type="email" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="password" class="col-sm-3 control-label">Password</label>
	                                        <div class="col-sm-9">
	                                            <g:passwordField name="password" class="form-control" placeholder="Pick a password" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="firstName" class="col-sm-3 control-label">Name</label>
	                                        <div class="col-sm-5">
	                                            <g:textField name="firstName" value="${user?.profile?.firstName}" class="form-control" placeholder="First name" required="true"/>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <g:textField name="lastName" value="${user?.profile?.lastName}" class="form-control" placeholder="Last name" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="yearOfBirth" class="col-sm-3 control-label">Year of birth</label>
	                                        <div class="col-sm-9">
	                                            <g:datePicker name="yearOfBirth" class="form-control" value="${user?.profile?.yearOfBirth}" precision="year" placeholder="Year of Birth" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="mobileNumber" class="col-sm-3 control-label">Contact Information</label>
	                                        <div class="col-sm-5">
	                                            <g:textField name="mobileNumber" value="${user?.profile?.mobileNumber}" class="form-control" placeholder="Mobile number" type="tel" required="true"/>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <g:textField name="landlineNumber" value="${user?.profile?.landlineNumber}" class="form-control" placeholder="Landline number" type="tel" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="aadharID" class="col-sm-3 control-label">Aadhar ID</label>
	                                        <div class="col-sm-9">
	                                            <g:textField name="aadharID" value="${user?.profile?.aadharID}" class="form-control" placeholder="Aadhar ID"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="category" class="col-sm-3 control-label">Category</label>
	                                        <div class="col-sm-9">
	                                            <g:select name="category" from="['Type 1 Diabetes - Self', 'Type 1 Diabetes - Mother', 'Type 1 Diabetes - Father', 'Type 1 Diabetes - Relative', 'Type 1 Diabetes - Friend', 'Type 2 Diabetes - Self', 'Type 2 Diabetes - Mother', 'Type 2 Diabetes - Father', 'Type 2 Diabetes - Relative', 'Type 2 Diabetes - Friend', 'Unrelated']" noSelection="['':'- Pick a category -']" value="${user?.profile?.category}" class="form-control" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="educationLevel" class="col-sm-3 control-label">Education</label>
	                                        <div class="col-sm-9">
	                                            <g:select name="educationLevel" from="['None', 'Primary Education', 'High School', 'Graduate', 'Post Graduate', 'Vocational Training']" noSelection="['':'- Choose Education -']" value="${user?.profile?.educationLevel}" class="form-control" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="address" class="col-sm-3 control-label">Address</label>
	                                        <div class="col-sm-9">
	                                            <g:textArea name="address" value="${user?.profile?.address}" rows="6" class="form-control" placeholder="Residential address" required="true"/>
	                                        </div>
	                                    </div>
	                                    <div class="form-group">
	                                        <label for="gender" class="col-sm-3 control-label">Gender</label>
	                                        <div class="col-sm-9">
	                                            <g:radioGroup name="gender" labels="['Male','Female']" values="['M','F']" required="true">
	                                                <div class="radio col-sm-4">
	                                                    <p>${it.label} ${it.radio}</p>
	                                                </div>
	                                            </g:radioGroup>
	                                        </div>
	                                    </div>
	                                    <!-- <div class="form-group">
	                                        <label for="role" class="col-sm-3 control-label">What is your Role</label>
	                                        <div class="col-sm-9">
	                                            <g:select name="role" from="['Community Health Worker', 'Data Entry Operator']" noSelection="['':'- Pick a role -']" class="form-control"/>
	                                        </div>
	                                    </div> -->
	                                    <div class="form-group">
	                                        <div class="col-sm-3 col-sm-offset-9">
	                                            <g:submitButton name="register" value="Sign up" class="btn btn-md btn-block btn-success"/>
	                                        </div>
	                                    </div>
	                                </g:form>
	                            </div>
	                        </div>
	                        <!-- /panel-body -->
	                    </div>
	                    <!-- /panel-footer -->
	                </div>
	            </div>
	            <!-- /panel-container div -->
	        </div>
        </div>
    </body>
</html>