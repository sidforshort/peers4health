<%@ page import="org.jnanasanjeevini.*" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="no_nav"/>
        <title>Thanks for Confirming your Sign-up! | Peers for Health</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'outcomes.css')}" type="text/css">
    </head>
    <body>
        <section style="background:#efefe9;">
            <div class="container-fluid">
                <div class="row">
                    <div class="board">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="thanks">
                                <h3 class="head text-center">Thanks for confirming your registration <span style="color:#f48260;">♥</span></h3>
                                <p class="text-center">
                                    <g:link controller="login" action="auth" class="btn btn-success btn-outline-rounded green"> Start using Peers for Health <span style="margin-left:10px;" class="glyphicon glyphicon-send"></span></g:link>
                                </p>
                            </div>
                            <!-- /thanks -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>