<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="layout" content="main"/>
		<title>Sign-up as a CHW | Peers for Health</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Loading Bootstrap -->
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<g:hasErrors bean="${user}">
					    <g:eachError><p><g:message error="${it}"/></p></g:eachError>
					</g:hasErrors>
				</div>
				<div class="col-md-12">
					<h4>Viewing All ${role} Users</h4>

					<div class="table-responsive">
		             	<table id="mytable" class="table table-bordred table-striped">
		                   	<thead>
		                   		<th>ID</th>
								<th>Full Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Edit</th>
								<th>Delete</th>
								<th>Add New Data</th>
							</thead>
							<tbody>
								<g:each in="${users}" var="user">
									<tr>
										<td>${user?.profile?.assignedId}</td>
										<td>${user?.profile?.firstName} ${user?.profile?.lastName}</td>
									    <td>${user?.email}</td>
									    <td>${user?.profile?.mobileNumber}</td>
									    <td><p><button class="btn btn-warning btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-pencil"></span></button></p></td>
										<td><p><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash"></span></button></p></td>
										<td><p><g:remoteLink class="btn btn-success btn-xs btn-block" action="show" id="1" update="[success:'mytable',failure:'error']" on404="alert('not found');"><span class="glyphicon glyphicon-plus-sign"></span>Add Data</g:remoteLink></p></td>
									</tr>
								</g:each>
							<tbody>
						</table>
					</div>					
				</div>
			</div>
		</div>
		<g:javascript src="list.js" />
	</body>
</html>