package org.jnanasanjeevini

import grails.transaction.Transactional

@Transactional
class EmailService {

	def mailService
    def grailsApplication

    def sendVerificationEmail (email, token) {

        def serverUrl = grailsApplication.config.grails.serverURL
        mailService.sendMail {
            to email
            subject "[Peers4Health] Complete your sign-up!"
            body "Click here to complete your sign-up: ${serverUrl}/user/confirm?token=${token}&email=${email}"
        }
    }

    def confirmEmail (email) {
        def user = User.findByEmail(email)
        if (user) {
            user.profile.emailConfirmed = true
            user.enabled = true
            user.save()
        }
    }

    def confirmMobile (mobileNumber) {
        def user = User.findByMobileNumber(mobileNumber)
        if (user) {
            user.mobileConfirmed = true
            user.enabled = true
            user.save()
        }
    }
}