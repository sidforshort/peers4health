package org.jnanasanjeevini

import grails.transaction.Transactional

class FamilyException extends RuntimeException {
    String message
    Family family
}

@Transactional
class FamilyService {

	def addFamily (params) {

		try {

            def family = new Family(params)

            if (family.validate() && family.save()) {
            	println "saved the family with id ${family.id}"
            	def headOfFamily = addHeadOfFamily (params, family)

            } else {
            	println "FAILED to save the family"
            	family.errors.each {
            		println it
            	}
            }

            return family
            
        } catch (FamilyException e) {
            log.error("An exception prevented saving the family information. Details:" + e)
            return null
        }
	}

	def addHeadOfFamily (params, family) {

		try {

            def headOfFamily = new FamilyHead(params)
            headOfFamily.family = family
            
            if (headOfFamily.validate() && headOfFamily.save()) {
            	println "saved the head of family with id ${headOfFamily.id}"

            } else {
            	println "FAILED to save the head of family"
            	headOfFamily.errors.each {
            		println it
            	}
            }

            return headOfFamily
            
        } catch (FamilyException e) {
            log.error("An exception prevented saving the head of family. Details:" + e)
            return null
        }
	}

    def getHeadOfFamily (int familyId) {

        def family = Family.get(familyId)
        def headOfFamily = Family.where {
            persons
        }

    }

    def isHeadOfFamily (Person person) {
    	(person.class == "FamilyHead") ? true : false
    }
}
