package org.jnanasanjeevini

import grails.transaction.Transactional
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import org.joda.time.*
import static java.util.UUID.randomUUID

class RegistrationException extends RuntimeException {
    String message
    User user
}

@Transactional
class RegistrationService {

    def emailService
    def springSecurityService

    def signup (map) {

        try {

            // check if the user exists
            def user = doesUserExist(email)
            
            if (user) {
                // send email to notify the user about the pricing for the route
                emailService.sendRouteDetailsEmail(user.email, user.profile.confirmationToken)

            } else {

                def hashedToken = springSecurityService.encodePassword(randomUUID() as String)
                def password = randomUUID() as String
                user = new User(map)
                if(!user.save(flush:true)) { user.errors.each { println it } }

                // assign a role to the newly created user
                UserRole.create user, Role.findByAuthority('ROLE_USER'), true

                // send the verification email to check if the registration is for real
                emailService.sendVerificationEmail(user.email, user.profile.confirmationToken)
            }

            return user
            
        } catch(Exception e) {
            log.error("An exception prevented persisting the user registration. Details:" + e)
            return null
        }
    }


    def doesUserExist (email) {
        def user = User.findByEmail(email)
        println "found user in the database: ${user}"
        return user
    }
}