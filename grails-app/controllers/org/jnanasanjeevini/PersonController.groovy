package org.jnanasanjeevini

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.util.*

@Secured(['ROLE_ADMIN', 'ROLE_CHW', 'ROLE_DATA'])
class PersonController {

    static scaffold = true
    def familyService

    def addFamilyMember () {

        println "addFamilyMember received params = ${params}"
        def family = Family.get(params.familyId?.toInteger())
        
        if (request.method == "POST") {

            println "family = ${family}"
            
            def person = new Person(params)
            person.family = family
            person.medicalHistory = new MedicalHistory(params)

            if (person.validate() && person.save()) {
                println "saved the person with id ${person.id}"
                flash.message = "This person was persisted successfully!"
                redirect(controller: "encounter", action: "addEncounter", params: [personId: person?.id])
                return
                
            } else {
                println "FAILED to save the person"
                person.errors.each {
                    println it
                }

                render(view: "addFamilyMember", model: [person: person, family: family])
            }
        }

        render(view: "addFamilyMember", model: [family: family, person: null])
    }

    def showFamilyMember (int id) {

    	println "showFamilyMember received params = ${params}"

        def person = Person.get(id)

        if (person) {
        	println "found person: ${person}"
        	render(view: "showFamilyMember", model: [person: person])

        } else {
        	flash.message = "This person was not found!"
        	render(view: "/")
        }
    }

    def editFamilyMember (int id) {
        
    }
}
