package org.jnanasanjeevini

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.util.*

@Secured(['ROLE_ADMIN', 'ROLE_CHW', 'ROLE_DATA'])
class FamilyController {

    static scaffold = true
    def familyService
    def springSecurityService

    /*def addFamily () {

    	println "addFamily received params = ${params}"
        def chwUser = springSecurityService.currentUser

        if (request.method == "POST") {

            def family = new Family(params)
            family.user = chwUser
            def headOfFamily = new FamilyHead(params)

            if (family.validate() && family.save()) {
                println "saved the family with id ${family.id}"
                headOfFamily.family = family
                headOfFamily.medicalHistory = new MedicalHistory(params)
                
                if (headOfFamily.validate() && headOfFamily.save()) {
                    println "saved the head of family with id ${headOfFamily.id}"
                    flash.message = "This family was persisted successfully!"
                    redirect(controller: "encounter", action: "addEncounter", params: [personId: headOfFamily?.id])
                    return

                } else {
                    println "FAILED to save the head of family"
                    headOfFamily.errors.each {
                        println it
                    }
                }

            } else {
                println "FAILED to save the family"
                family.errors.each {
                    println it
                }

                render(view: "addFamily", model: [family: family, user: chwUser, familyHead: headOfFamily])
                return
            }

        } else {
            render(view: "addFamily", model: [user: chwUser])
        }
        
    }*/


    def addFamily () {

        println "addFamily received params = ${params}"
        def chwUser = springSecurityService.currentUser

        if (request.method == "POST") {

            def family = new Family(params)
            family.user = chwUser
            
            def headOfFamily = new FamilyHead(params)
            headOfFamily.family = family
            headOfFamily.medicalHistory = new MedicalHistory(params)

            if (family.validate() && family.save()) {
                println "saved the family with id ${family.id}"
                
                if (headOfFamily.validate() && headOfFamily.save()) {
                    println "saved the head of family with id ${headOfFamily.id}"
                    flash.message = "This family was persisted successfully!"
                    redirect(controller: "encounter", action: "addEncounter", params: [personId: headOfFamily?.id])
                    return

                } else {
                    println "FAILED to save the head of family"
                    headOfFamily.errors.each {
                        println it
                    }
                }

            } else {
                println "FAILED to save the family"
                family.errors.each {
                    println it
                }

                render(view: "addFamily", model: [family: family, user: chwUser, familyHead: headOfFamily])
                return
            }

        } else {
            render(view: "addFamily", model: [user: chwUser, familyHead: null])
        }
        
    }


    def editFamily (int id) {

        if (request.method == "POST") {

            def family = Family.get(id)
            family = Family(params)
            def headOfFamily = FamilyHead(params)

            if (family.validate() && family.save()) {
                println "saved the family with id ${family.id}"
                headOfFamily.family = family
                headOfFamily.medicalHistory = MedicalHistory(params)
                
                if (headOfFamily.validate() && headOfFamily.save()) {
                    println "saved the head of family with id ${headOfFamily.id}"
                    flash.message = "This family was persisted successfully!"
                    redirect(controller: "encounter", action: "addEncounter", params: [personId: headOfFamily?.id])
                    return

                } else {
                    println "FAILED to save the head of family"
                    headOfFamily.errors.each {
                        println it
                    }
                }

            } else {
                println "FAILED to save the family"
                family.errors.each {
                    println it
                }

                render(view: "addFamily", model: [family: family, user: chwUser, familyHead: headOfFamily])
                return
            }

        } else {
            render(view: "addFamily", model: [user: chwUser])
        }

    }


    def showFamily (int id) {

    	println "showFamily received params = ${params}"

        def family = Family.get(id)

        if (family) {
        	println "found family: ${family}"
        	render(view: "showFamily", model: [family: family])

        } else {
        	flash.message = "This family was not found!"
        	render(view: "/")
        }
    }

    def myFamilies () {

        println "myFamilies received params = ${params}"

        def user = springSecurityService.currentUser
        def families = Family.where { user == user }
        println "user : ${user}"
        println "families : ${families}"
        render(view: "myFamilies", model: [families: families, user: user])
    }
}
