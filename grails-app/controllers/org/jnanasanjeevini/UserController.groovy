package org.jnanasanjeevini

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import static java.util.UUID.randomUUID
import grails.converters.JSON
import grails.util.*

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN', 'ROLE_ANONYMOUS'])
class UserController {

    static scaffold = true

    def registrationService
    def springSecurityService
    def emailService

    @Secured(['ROLE_ANONYMOUS','ROLE_ADMIN','ROLE_USER'])
    def fail() {
        render(view: "fail")
    }

    @Secured(['ROLE_DATA','ROLE_ADMIN','ROLE_USER'])
    def listUsers() {
        def dataUsers = UserRole.findAllByRole(Role.findByAuthority('ROLE_DATA'))?.user
        render(view: "list", model: [users:dataUsers, role:"Data Entry"])
    }

    @Secured(['ROLE_DATA','ROLE_ADMIN','ROLE_USER'])
    def listChws() {
        def dataUsers = UserRole.findAllByRole(Role.findByAuthority('ROLE_CHW'))?.user
        render(view: "list", model: [users:dataUsers, role:"CHW"])
    }

    @Transactional
    @Secured(['ROLE_ANONYMOUS','ROLE_ADMIN','ROLE_USER'])
    def register() {

        println "register received params = ${params}"

        if (request.method != "POST") {
            render(view: "register", model: [user: null])
            return
        }
        
        def user = new User(params)
        def profile = new Profile(params)
        def confirmationToken = springSecurityService.encodePassword(randomUUID() as String)
        profile.confirmationToken = confirmationToken
        user.profile = profile

        if (user.validate() && user.save()) {
            /*if (params.role == "Community Health Worker") {
                UserRole.create user, Role.findByAuthority('ROLE_CHW'), true
                user.profile.assignedId = "CHW-${user.id}"
            } else {
                UserRole.create user, Role.findByAuthority('ROLE_DATA'), true
                user.profile.assignedId = "USR-${user.id}"
            }*/
            
            UserRole.create user, Role.findByAuthority('ROLE_CHW'), true
            user.profile.assignedId = "CHW-${user.id}"
            user.save()

            flash.message = "This user was persisted successfully!"
            emailService.sendVerificationEmail(params.email, confirmationToken)
            render(view: "thanks")

        } else {
            flash.message = "This user was NOT persisted!"
            user.errors.each {
                println it
            }

            render(view: "register", model: [user: user])
        }
    }

    @Transactional
    @Secured(['ROLE_ANONYMOUS','ROLE_ADMIN','ROLE_USER'])
    def confirm () {

        if (params.token) {
            def profile = Profile.where { confirmationToken: params.token }
            println "profile = ${profile}"
            // send an email with instuctions on setting a new password
            emailService.confirmEmail(params.email)
            render(view: "confirm")

        } else {
            render view:"register"
        }
    }

    @Transactional
    @Secured(['ROLE_ANONYMOUS','ROLE_ADMIN','ROLE_USER'])
    def thanks() {
        render(view: "thanks")
    }

    def renderImage() {
        def user = User.findByEmail (params.email)
        if (user?.profile?.photo) {
            response.setContentLength(user.profile.photo.size())
            response.outputStream.write(user.profile.photo)
        } else {
            response.sendError(404)
        }
    }
}

class UserRegistrationCommand {
    
    String email
    String password
    String confirmPassword
    String firstName
    String lastName
    String mobileNumber
    String aadharID

    static constraints = {
        //importFrom Profile
        importFrom User
        confirmPassword (nullable: false, validator: { passwd2, urc ->
            return passwd2 == urc.password
        })
    }
}