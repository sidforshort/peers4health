package org.jnanasanjeevini

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import grails.util.*

@Secured(['ROLE_ADMIN', 'ROLE_CHW', 'ROLE_DATA'])
class EncounterController {

    static scaffold = true
    def familyService

    def addEncounter () {

        println "addEncounter received params = ${params}"
        if (request.method != "POST") {
            render (view: "addEncounter", model: [person: Person.get(params.personId?.toInteger())])
            return
        }
    	
    	def encounter = new Encounter(params)
    	def person = Person.get(params.personId?.toInteger())
        encounter.person = person

        if (encounter.validate() && encounter.save()) {
        	println "saved the encounter with id ${encounter.id}"
        	flash.message = "This encounter was persisted successfully!"
            redirect(action: "addPhysicalExam", params: [encounterId: encounter.id])

        } else {
        	println "FAILED to save the encounter"
        	encounter.errors.each {
        		println it
        	}

            render(view: "addEncounter", model: [person: Person.get(params.personId?.toInteger()), encounter: encounter])
        }
    }


    def showEncounters () {

        println "showEncounters received params = ${params}"
        
        def person = Person.get(params.personId?.toInteger())
        def encounters = Encounter.where { person == person }
        println "encounters = ${encounters}"
        render(view: "showEncounters", model: [person: Person.get(params.personId?.toInteger()), encounters: encounters])
    }


    def viewEncounter (long id) {
        println "viewEncounter received params = ${params}"
        def encounter = Encounter.get(id)
        println "encounter = ${encounter}"
        render(view: "viewEncounter", model: [encounter: encounter])
    }


    def addPhysicalExam () {

    	println "addPhysicalExam received params = ${params}"
        if (request.method != "POST") {
            render (view: "addPhysicalExam", model: [encounterId: params.encounterId])
            return
        }

    	def encounter = Encounter.get(params.encounterId?.toInteger())
    	def physicalExam = new PhysicalExamination(params)
        physicalExam.encounter = encounter

        if (physicalExam.validate() && physicalExam.save()) {
        	println "saved the physicalExam with id ${physicalExam.id}"
        	flash.message = "This physical Exam was persisted successfully!"
        	redirect(action: "addDiabetesExam", params: [encounterId: encounter.id])

        } else {
        	println "FAILED to save the physicalExam"
        	physicalExam.errors.each {
        		println it
        	}

        	render(view: "addPhysicalExam", model: [encounterId: params.encounterId, physicalExam: physicalExam])
        }
    }

    def addDiabetesExam () {

    	println "addDiabetesExam received params = ${params}"
        if (request.method != "POST") {
            render (view: "addDiabetesExam", model: [encounterId: params.encounterId])
            return
        }

    	def encounter = Encounter.get(params.encounterId.toInteger())
    	def diabetesExam = new DiabetesExamination(params)
        diabetesExam.encounter = encounter

        if (diabetesExam.validate() && diabetesExam.save()) {
        	println "saved the diabetesExam with id ${diabetesExam.id}"
        	flash.message = "This diabetesExam was persisted successfully!"
        	redirect(action: "addHypertensionExam", params: [encounterId: encounter.id])

        } else {
        	println "FAILED to save the diabetesExam"
        	diabetesExam.errors.each {
        		println it
        	}

        	render(view: "addDiabetesExam", model: [encounterId: params.encounterId, diabetesExam: diabetesExam])
        }
    }

    def addHypertensionExam () {

    	println "addHypertensionExam received params = ${params}"
        if (request.method != "POST") {
            render (view: "addHypertensionExam", model: [encounterId: params.encounterId])
            return
        }

    	def encounter = Encounter.get(params.encounterId.toInteger())
    	def hypertensionExam = new HypertensionExamination(params)
        hypertensionExam.encounter = encounter

        if (hypertensionExam.validate() && hypertensionExam.save()) {
        	println "saved the hypertensionExam with id ${hypertensionExam.id}"
        	flash.message = "This hypertensionExam was persisted successfully!"
        	redirect(action: "addOutcome", params: [encounterId: encounter.id])

        } else {
        	println "FAILED to save the hypertensionExam"
        	hypertensionExam.errors.each {
        		println it
        	}

        	render(view: "addHypertensionExam", model: [encounterId: params.encounterId, hypertensionExam: hypertensionExam])
        }
    }

    def addOutcome () {

        println "addOutcome received params = ${params}"
        if (request.method != "POST") {
            render (view: "addOutcome", model: [encounterId: params.encounterId])
            return
        }

        def encounter = Encounter.get(params.encounterId.toInteger())
        def screeningOutcome = new ScreeningOutcome(params)
        screeningOutcome.encounter = encounter

        if (screeningOutcome.validate() && screeningOutcome.save()) {
            println "saved the screeningOutcome with id ${screeningOutcome.id}"
            flash.message = "This screeningOutcome was persisted successfully!"

            /*if (screeningOutcome.validate() && screeningOutcome.save()) {
                println "saved the screeningOutcome with prognostications"
                return

            } else {
                println "FAILED to save the screeningOutcome with prognostications"
                screeningOutcome.errors.each {
                    println it
                }
            }*/

            redirect(action: "viewEncounter", id:encounter.id)
            return

        } else {
            println "FAILED to save the screeningOutcome"
            screeningOutcome.errors.each {
                println it
            }

            render(view: "addOutcome", model: [encounterId: params.encounterId, screeningOutcome: screeningOutcome])
        }
    }


    def showEncounter (int id) {

    	println "showEncounter received params = ${params}"

        def encounter = Encounter.get(id)

        if (encounter) {
        	println "found Encounter: ${encounter}"
        	render(view: "showEncounter", model: [encounter: encounter])

        } else {
        	flash.message = "This encounter was not found!"
        	render(view: "/")
        }
    }
}
